Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |  [de](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/01_operating_manual/S3-0023_A2_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/01_operating_manual/S3-0023_A2_Operating%20Manual.pdf)                     |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/02_assembly_drawing/S3-0023_A_ZNB_Drip_Tray_DT-300.PDF)                  |
| circuit diagram          |                  |
| maintenance instructions |[de](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/04_maintenance_instructions/S3-0023_A_Wartungsanweisungen.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/04_maintenance_instructions/S3-0023_A_Maintenance%20instructions.pdf)                   |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/05_spare_parts/S3-0023_A1_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0023-drip-tray-dt-300/-/raw/main/05_spare_parts/S3-0023_A1_EVL_engl.pdf)                   |

